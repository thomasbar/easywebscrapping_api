from requests import get
from flask import Flask, jsonify
from bs4 import BeautifulSoup

from .conf import newspapers

application = Flask(__name__)

@application.route('/', methods=['GET'])
def welcome():
    """
    """
    return "<h1>Welcome to my API!</h1>"

@application.route('/news/<word>', methods=['GET'])
def news(word):
    """
    """
    resp = []
    for newspaper in newspapers:
        articles = BeautifulSoup(get(newspaper["address"]).content, "html.parser").find_all('a')
        resp = resp + [{
            "title": article.get_text(),
            "url": article["href"],
            "source": newspaper["name"]
            } for article in articles if word in article.get_text()]
    return jsonify(resp)