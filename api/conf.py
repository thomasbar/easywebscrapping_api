newspapers = [
        {
        "name": "Le Parisien",
        "address": "https://www.leparisien.fr/"
    },
    {
        "name": "Le Figaro",
        "address": "https://www.lefigaro.fr/"
    },
    {
        "name": "Liberation",
        "address": "https://www.liberation.fr/"
    },
    {
        "name": "L'Humanite",
        "address": "https://www.humanite.fr/"
    },
    {
        "name": "Mediapart",
        "address": "https://www.mediapart.fr/"
    },
    {
        "name": "20 Minutes",
        "address": "https://www.20minutes.fr/"
    },
    {
        "name": "Metro",
        "address": "https://journalmetro.com/"
    }
]